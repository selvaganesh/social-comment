import { Component, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';


@Component({
    selector: 'context-menu',
    templateUrl: './context-menu-component.html',
    styleUrls: ['./context-menu.css']
})
export class ContextMenuComponent implements OnChanges {

    contextactive;
    yposition;
    xposition;

    @Output() contextEvent = new EventEmitter();

    @Input() set contextActive(value){
        this.contextactive = value;
    }

    @Input()set x(value){
        this.xposition = value;
    }

    @Input()set y(value){
        this.yposition = value;
    }
    
    constructor() {

    }

    onClickInsert(event){
        this.contextEvent.emit(event);
    }

    ngOnChanges(sc: SimpleChanges) {
        console.log('sdfdf' + sc);
        if (sc['active']) {
            // this.calculatePositionComment();
        }
    }


}
