import { Component, Input, OnChanges, SimpleChanges, EventEmitter, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
    selector: 'social-comment',
    templateUrl: './social-comment-component.html',
    styleUrls: ['./social-comment.css']
})
export class SocialCommentComponent implements OnChanges {

    active;
    comment = '';
    commentList = [];

    @Input() set socialactive(value){
        this.active = value;
        this.commentList = [];
    }

    @Output() cancelClick = new  EventEmitter();
    @Output() commentClick = new EventEmitter();
    constructor() {

    }

    ngOnChanges(sc: SimpleChanges) {
        console.log('sdfdf' + sc);
        if (sc['active']) {
            this.calculatePositionComment();
        }
    }

    onCancel(event) {
        this.cancelClick.emit();
        this.comment = '';
    }

    onComment($event){
        const comment = this.comment;
        this.commentList.push(comment);
        this.comment = '';
        this.commentClick.emit();
    }
    private calculatePositionComment = () => {

    }
}
