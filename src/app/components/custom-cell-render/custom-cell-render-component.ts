import { Component, Input, OnChanges, SimpleChanges, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { MatMenuTrigger } from '@angular/material/menu';
import { stringify } from 'querystring';

@Component({
    selector: 'custom-cell-render',
    template: `<div *ngIf="editable" class="input-wrapper"><input type="number" 
    [(ngModel)]="value" class="custom-input" 
    (contextmenu)="onContextMenu($event)" />
    <span [class.social-comment-marker]="socialCommentActive"
    (mouseover)="onMouseOver($event)" (mouseout)="onMouseOut($event)"></span></div>
    <div *ngIf="!editable" (contextmenu)="onContextMenu($event)">{{value}}
    <span *ngIf="socialCommentActive" [class.social-comment-marker]="socialCommentActive" [class.cell-noneditable]="socialCommentActive"
    (mouseover)="onMouseOver($event)" (mouseout)="onMouseOut($event)"></span>
    </div>
    `,
    styleUrls: ['./custom-cell-render.css']
})
export class CustomCellRenderComponent implements ICellRendererAngularComp {
    
    contextActive = false;
    contextmenuX = 0;
    contextmenuY = 0;
    public params: any;
    socialCommentActive = false;
    value;
    editable;
    agInit(params: any): void {
        this.params = params;
        let age = params.value.toString();
        let data = age.split("-");
        this.editable = params.colDef.editable;
        if(data.length === 2){
            this.value = parseInt(data[0]);  
            this.socialCommentActive = true;
            return;
        }
        this.value = params.data.age;
    }

    public onMouseOver(event) {
        this.params.context.componentParent.onCellMouseOver(`${event.pageX},${this.params.rowIndex}`);
        //this.params.context.componentParent.onCellMouseOver(`event: ${event}, params: ${this.params}`);
    }

    public onMouseOut(event) {
        this.params.context.componentParent.onCellMouseOut(`event: ${event}, params: ${this.params}`);
    }

    onContextMenu(event){
        event.preventDefault;
        // this.socialCommentActive = true;
        let value = {};
        value['event'] = event;
        value['params'] = this.params;
        this.params.context.componentParent.onContextMenuOut(value);
        // this.contextmenuX = event.clientX;
        // this.contextmenuY = event.clientY;
        // this.contextActive = true;
         return false;
    }

    refresh(): boolean {
        return false;
    }
}
