import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { AgGridModule } from 'ag-grid-angular';
import { HttpClientModule } from "@angular/common/http";
import { SocialCommentComponent } from './components/social-comment/social-comment-component';
import { CustomCellRenderComponent } from './components/custom-cell-render/custom-cell-render-component';
import {MatMenuModule} from '@angular/material/menu';
import { ContextMenuComponent } from './components/context-menu/context-menu-component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    SocialCommentComponent,
    CustomCellRenderComponent,
    ContextMenuComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatMenuModule,
    FormsModule,
    AgGridModule.withComponents([CustomCellRenderComponent])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
